import boto3
import os

def list_functions():
    session = boto3.Session(
        aws_access_key_id=os.getenv('AWS_ACCESS_KEY_ID'),
        aws_secret_access_key=os.getenv('AWS_SECRET_ACCESS_KEY'),
        region_name=os.getenv('AWS_REGION')
    )
    lambda_client = session.client('lambda')

    response = lambda_client.list_functions()
    functions = response['Functions']

    for function in functions:
        print(function['FunctionName'])

list_functions()
